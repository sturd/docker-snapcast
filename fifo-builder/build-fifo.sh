#!/bin/sh

# Clean out the pipes directory
rm -rf /var/lib/pipes/*

# MPD FIFO
mkfifo /var/lib/pipes/mpdfifo

# Shairport-Sync FIFO
mkfifo /var/lib/pipes/spsfifo

# Shift ownership to user
chown -R fifouser:fifogroup /var/lib/pipes/