#!/bin/sh

while [ ! -e /var/lib/mpd/pipes/mpdfifo ];
do
    echo "Unable to find file, retrying in 5 seconds..."
    sleep 5
done

/usr/bin/mpd --stdout --no-daemon /var/lib/mpd/mpd.conf
